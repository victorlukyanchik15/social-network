package com.senla.social.authentificate;

import com.senla.social.entity.Profile;

public interface ProfileAuthenticator {

    Profile getProfile();
}
