package com.senla.social.converter;

import com.senla.social.dto.message.AllMessageDto;
import com.senla.social.entity.Message;
import org.springframework.stereotype.Component;

@Component
public class MessageConverter {

    public AllMessageDto convert(Message message, String name){
        AllMessageDto result = new AllMessageDto();
        result.setName(name);
        result.setText(message.getText());
        result.setDateTime(message.getDateTime());
        return result;
    }
}
