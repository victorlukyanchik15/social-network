package com.senla.social.dto.message;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class AllMessageDto {

    private String text;

    private String name;

    private LocalDateTime dateTime;
}
