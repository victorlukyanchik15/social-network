package com.senla.social.dto.message;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class MessageSendDto {

    private Long receiverId;

    @Size(max = 255, message = "Message should be less than 255")
    @NotBlank(message = "Message text can't be empty")
    private String text;
}
