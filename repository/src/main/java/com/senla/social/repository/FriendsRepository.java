package com.senla.social.repository;

import com.senla.social.entity.Friends;
import com.senla.social.entity.Profile;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface FriendsRepository extends JpaRepository<Friends, Long> {
    List<Friends> findAllBySender(Profile sender);

    Optional<Friends> findById(Long id);
}
