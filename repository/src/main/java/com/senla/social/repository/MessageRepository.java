package com.senla.social.repository;

import com.senla.social.entity.Friends;
import com.senla.social.entity.Message;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MessageRepository extends JpaRepository<Message, Long> {

    List<Message> findAllByFriends(Friends friends);
}
