package com.senla.social.repository;

import com.senla.social.entity.Profile;
import com.senla.social.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ProfileRepository extends JpaRepository<Profile, Long> {

    Profile findProfileByUser(User user);

    Optional<Profile> findById(Long id);
}
