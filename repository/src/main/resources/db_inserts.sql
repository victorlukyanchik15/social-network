INSERT INTO roles (name)
values ('ROLE_USER'),
       ('ROLE_ADMIN'),
       ('ROLE_GROUP_MANAGER');

INSERT INTO users (username, password)
VALUES ('user', '$2a$12$l7R59Z4EzXqzPwKB1wEhU.QMdvp.ITrKlEN5SJkQRaSriVwxbeNlG'),
       ('admin', '$2a$12$ObOzgB2NDmPgrOJAFEitre0vrZnZRkxrnmscmYoUWXozgbGhLFAsC');

INSERT INTO profiles (firstname, lastname, email, age, user_id)
VALUES ('user', 'user', 'user@gmail.com', '18', 1),
       ('admin', 'admin', 'admin@gmail.com', '18', 2);

INSERT INTO user_roles (user_id, role_id)
VALUES (1, 1),
       (2, 2);

INSERT INTO interests (name)
VALUES ('musics'),
       ('films'),
       ('studies'),
       ('sport'),
       ('travel'),
       ('shopping'),
       ('animals'),
       ('business'),
       ('other');