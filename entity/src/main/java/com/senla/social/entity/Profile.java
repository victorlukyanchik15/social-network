package com.senla.social.entity;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.List;

@Entity
@Table(name = "profiles")
@SequenceGenerator(
        name = "profiles-gen",
        sequenceName = "profiles_id_seq",
        initialValue = 1, allocationSize = 1)
public class Profile {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "profiles-gen")
    private Long id;

    @Column(name = "firstname")
    @Size(min = 2, max = 50, message = "Firstname should be more than 2 and less than 50")
    @NotBlank(message = "Firstname can't be empty")
    @Pattern(regexp = "^[A-Z][a-z0-9_-]{3,19}$", message = "First letter in firstname should be uppercase")
    private String firstname;

    @Column(name="lastname")
    @Size(min = 2, max = 50, message = "Lastname should be more than 2 and less than 50")
    @NotBlank(message = "Lastname can't be empty")
    @Pattern(regexp = "^[A-Z][a-z0-9_-]{3,19}$", message = "First letter in lastname should be uppercase")
    private String lastname;

    @Column(name= "email")
    @NotBlank(message = "Email can't be empty")
    @Email(message = "Email should be valid")
    @Size(max = 100, message = "Email should be less than 100")
    private String email;

    @Column(name = "sex")
    @Size(max=15, message = "Sex should be more than 15")
    @Pattern(regexp = "FEMALE|MALE|not specified")
    private String sex;

    @Column(name = "age")
    @Min(value = 6, message = "Age should be more than 6")
    @Max(value = 120, message = "Age should less than 120")
    private Integer age;

    @Column(name = "town")
    @Size(min = 2, max = 50, message = "Town name should be more than 2 and less than 50")
    @Pattern(regexp = "^[A-Z][a-z0-9_-]{3,19}$", message = "First letter in town should be uppercase")
    private String town;

    @Column(name = "phone")
    @Size(min = 7, max = 15, message = "Phone should be more than 7 and less than 15")
    private String phone;

    @Column(name = "family_status")
    @Size(max = 50, message = "Family status should be less than 50")
    private String familyStatus;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "profile_groups",
            joinColumns = {@JoinColumn(name = "profile_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "group_id", referencedColumnName = "id")})
    private List<Group> groups;

    public Profile(Long id, String firstname, String lastname, String email, String sex,
                   Integer age, String town, String phone, String familyStatus, User user, List<Group> groups) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.sex = sex;
        this.age = age;
        this.town = town;
        this.phone = phone;
        this.familyStatus = familyStatus;
        this.user = user;
        this.groups = groups;
    }

    public Profile(String firstname, String lastname, String email, String sex,
                   Integer age, String town, String phone, String familyStatus, User user, List<Group> groups) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.sex = sex;
        this.age = age;
        this.town = town;
        this.phone = phone;
        this.familyStatus = familyStatus;
        this.user = user;
        this.groups = groups;
    }

    public Profile(String firstname, String lastname, String email, String sex,
                   Integer age, String town, String phone, String familyStatus, User user) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.sex = sex;
        this.age = age;
        this.town = town;
        this.phone = phone;
        this.familyStatus = familyStatus;
        this.user = user;
    }

    public Profile(Long id, String firstname, String lastname, String email, Integer age) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.age = age;
    }

    public Profile(String firstname, String lastname, String email, Integer age) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.age = age;
    }

    public Profile() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFamilyStatus() {
        return familyStatus;
    }

    public void setFamilyStatus(String familyStatus) {
        this.familyStatus = familyStatus;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }
}
