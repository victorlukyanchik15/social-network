package com.senla.social.entity;

import javax.persistence.*;

@Entity
@Table(name = "friends")
@SequenceGenerator(
        name = "friends-gen",
        sequenceName = "friends_id_seq",
        initialValue = 1, allocationSize = 1)
public class Friends {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "friends-gen")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sender_id")
    private Profile sender;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "receiver_id")
    private Profile receiver;

    public Friends(Long id, Profile sender, Profile receiver) {
        this.id = id;
        this.sender = sender;
        this.receiver = receiver;
    }

    public Friends(Profile sender, Profile receiver) {
        this.sender = sender;
        this.receiver = receiver;
    }

    public Friends() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Profile getSender() {
        return sender;
    }

    public void setSender(Profile sender) {
        this.sender = sender;
    }

    public Profile getReceiver() {
        return receiver;
    }

    public void setReceiver(Profile receiver) {
        this.receiver = receiver;
    }
}
