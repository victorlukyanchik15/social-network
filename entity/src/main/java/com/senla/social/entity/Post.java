package com.senla.social.entity;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(name = "posts")
@SequenceGenerator(
        name = "posts-gen",
        sequenceName = "posts_id_seq",
        initialValue = 1, allocationSize = 1)
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "posts-gen")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "group_id")
    private Group group;

    @Column(name = "title")
    @Size(max = 80, message = "Post title should be less than 80")
    @NotBlank(message = "Post title can't be empty")
    private String title;

    @Column(name = "text")
    @Size(max = 255, message = "Post text should be less than 255")
    @NotBlank(message = "Post text can't be empty")
    private String text;

    public Post(Long id, Group group, String title, String text) {
        this.id = id;
        this.group = group;
        this.title = title;
        this.text = text;
    }

    public Post(Group group, String title, String text) {
        this.group = group;
        this.title = title;
        this.text = text;
    }

    public Post(String title, String text) {
        this.title = title;
        this.text = text;
    }

    public Post() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
