package com.senla.social.entity;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "messages")
@SequenceGenerator(
        name = "messages-gen",
        sequenceName = "messages_id_seq",
        initialValue = 1, allocationSize = 1)
public class Message {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "messages-gen")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "friends_id")
    private Friends friends;

    @Column(name = "text")
    @Size(max = 255, message = "Message should be less than 255")
    @NotBlank(message = "Message text can't be empty")
    private String text;

    @Column(name = "date_time")
    @NotNull(message = "Date and time can't be null")
    private LocalDateTime dateTime;

    public Message(Long id, Friends friends, String text, LocalDateTime dateTime) {
        this.id = id;
        this.friends = friends;
        this.text = text;
        this.dateTime = dateTime;
    }

    public Message(Friends friends, String text, LocalDateTime dateTime) {
        this.friends = friends;
        this.text = text;
        this.dateTime = dateTime;
    }

    public Message(Friends friends, String text) {
        this.friends = friends;
        this.text = text;
    }

    public Message() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Friends getFriends() {
        return friends;
    }

    public void setFriends(Friends friends) {
        this.friends = friends;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, friends, text, dateTime);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message = (Message) o;
        return Objects.equals(id, message.id) &&
                Objects.equals(friends, message.friends) &&
                Objects.equals(text, message.text) &&
                Objects.equals(dateTime, message.dateTime);
    }
}
