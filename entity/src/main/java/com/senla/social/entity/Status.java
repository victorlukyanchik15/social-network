package com.senla.social.entity;

public enum Status {
    ACTIVE, NOT_ACTIVE, DELETED
}
