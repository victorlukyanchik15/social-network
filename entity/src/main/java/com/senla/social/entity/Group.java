package com.senla.social.entity;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "groups")
@SequenceGenerator(name = "groups-gen", sequenceName = "groups_id_seq", initialValue = 1, allocationSize = 1)
public class Group {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "groups-gen")
    private Long id;

    @Column(name = "name")
    @Size(max = 50, message = "Group name should be less than 50")
    @NotBlank(message = "Group name can't be empty")
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "interest_id")
    private Interest interest;

    @ManyToMany(mappedBy = "groups", fetch = FetchType.LAZY)
    private List<Profile> profiles;

    public Group(Long id, String name, Interest interest, List<Profile> profiles) {
        this.id = id;
        this.name = name;
        this.interest = interest;
        this.profiles = profiles;
    }

    public Group(String name, Interest interest, List<Profile> profiles) {
        this.name = name;
        this.interest = interest;
        this.profiles = profiles;
    }

    public Group(Long id, String name, Interest interest) {
        this.id = id;
        this.name = name;
        this.interest = interest;
    }

    public Group(String name, Interest interest) {
        this.name = name;
        this.interest = interest;
    }

    public Group() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Interest getInterest() {
        return interest;
    }

    public void setInterest(Interest interest) {
        this.interest = interest;
    }

    public List<Profile> getProfiles() {
        return profiles;
    }

    public void setProfiles(List<Profile> profiles) {
        this.profiles = profiles;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, interest, profiles);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Group group = (Group) o;
        return Objects.equals(id, group.id) &&
                Objects.equals(name, group.name) &&
                Objects.equals(interest, group.interest) &&
                Objects.equals(profiles, group.profiles);
    }
}
