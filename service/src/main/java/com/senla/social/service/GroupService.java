package com.senla.social.service;

import com.senla.social.entity.Group;

public interface GroupService {

    void create(Group group);

    void update(Group group);

    Group findById(Long id);
}
