package com.senla.social.service;

import com.senla.social.entity.Friends;
import com.senla.social.entity.Profile;

import java.util.List;

public interface FriendsService {

    Friends create(Friends friends);

    List<Friends> findAllBySender(Profile sender);

    Friends findById(Long id);
}
