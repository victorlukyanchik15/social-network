package com.senla.social.service;

import com.senla.social.entity.Friends;
import com.senla.social.entity.Message;

import javax.validation.Valid;
import java.util.List;

public interface MessageService {

    void create(Message message);

    void update(@Valid Message message);

    List<Message> findAllByFriends(Friends friends);
}
