package com.senla.social.service;

import com.senla.social.entity.Interest;

public interface InterestService {

    void create(Interest interest);

    Interest findByName(String name);
}
