package com.senla.social.service;

import com.senla.social.entity.Profile;
import com.senla.social.entity.User;

import java.util.List;

public interface UserService {

    User register(User user, Profile profile);

    User findByUsername(String username);

    User findById(Long id);

    User updateRoleGroupManager(User user);
}
